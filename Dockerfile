FROM registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-20.04:latest
MAINTAINER rleigh@codelibre.net

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get remove -y -qq --no-install-recommends \
    libxalan-c-dev \
    libxalan-c111 \
    xalan
